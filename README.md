# TD modèle gravitaire

## Description

Dépot pour la préparation d'une feuille de TD sur le modèle gravitaire à partir des données sur les investissements immobiliers à Dubaï. 

Le projet vise à adapter un TD d'analyse spatiale utilisé à Paris 7 avec les données de l'article de [Alstadsæter et al (2022)]([Who owns offshore real estate? | Gabriel Zucman](https://gabriel-zucman.eu/who-owns-offshore-real-estate/)) sur les possessions immobilières des étrangers à Dubaï.

#### Roadmap

Peut être ajouter la variable de la langue commune à partir des *correlates of war*.

Problème : du fait des distributions, il est nécessaire de passer par des logarithmes dans les explo bi-variées puis dans le modèle. C'est du coup moins pédagogique comme exemple. 

## Auteurs

Antoine Peris, Pierre Le Brun

## License

A définir
